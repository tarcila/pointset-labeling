#include "Viewer3DPrivate.hpp"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QQuickWindow>
#include <QTimer>
#include <QThread>

#include "OpenGLLogger.hpp"

#include <half.hpp>

#include <iterator>
#include <algorithm>

Viewer3DPrivate::Viewer3DPrivate(Viewer3D *parent) :
    q_ptr(parent),
    m_frameBuffer(0),
    m_drawPointSetProgram(0),
    m_pointSetVao(0),

    m_antialiasingProgram(0),
    m_antialiasingTexture(0),
    m_normalizedQuadVao(0),

    m_logger(this),
    m_shadersChanged(true),
    m_sourceWatcher(new QFileSystemWatcher(this)) {
}

void Viewer3DPrivate::compileShader(QString fileName, GLuint shaderObject)
{
    QFile shaderFile(fileName);

    shaderFile.open(QFile::ReadOnly);
    QByteArray shaderCode = shaderFile.readAll();
    const char* p = shaderCode.constData();
    glShaderSource(shaderObject, 1, &p, nullptr);
    glCompileShader(shaderObject);
    char buffer[4096];
    GLsizei length = 0;
    glGetShaderInfoLog(shaderObject, sizeof(buffer) - 1, &length, buffer);
    buffer[length] = '\0';
    qDebug() << fileName << " compile log:\n" << buffer;
}

void Viewer3DPrivate::doConnect()
{
    Q_Q(Viewer3D);

    connect(q->window(), SIGNAL(beforeSynchronizing()), this, SLOT(synchronize()), Qt::DirectConnection);
    connect(q->window(), SIGNAL(sceneGraphInitialized()), this, SLOT(init()), Qt::DirectConnection);
    connect(q->window(), SIGNAL(beforeRendering()), this, SLOT(paint()), Qt::DirectConnection);
    connect(q->window(), SIGNAL(sceneGraphAboutToStop()), this, SLOT(finish()), Qt::DirectConnection);

    connect(q, &Viewer3D::fileNameChanged, [&]() { m_fileNameChanged = true; });
}

void Viewer3DPrivate::doDisconnect()
{
    disconnect(this, SLOT(finish()));
    disconnect(this, SLOT(paint()));
    disconnect(this, SLOT(init()));
}

void Viewer3DPrivate::init() {
    Q_Q(Viewer3D);

    m_viewport = QRect(q->x(), q->window()->height() - q->height() + q->y(), q->width(), q->height());

    glewInit();

    m_logger.initialize();

    // Create quad used to support post rendering effect
    GLuint vertexBuffer;
    glCreateBuffers(1, &vertexBuffer);
    QVector2D vertices[] = {{1.0f, -1.0f}, {-1.0f, -1.0f}, {1.0f, 1.0f}, {-1.0f, 1.0f}};
    glNamedBufferStorage(vertexBuffer, 4 * sizeof(QVector2D), vertices, 0);

    glCreateVertexArrays(1, &m_normalizedQuadVao);
    glEnableVertexArrayAttrib(m_normalizedQuadVao, 0);
    glVertexArrayVertexBuffer(m_normalizedQuadVao, 0, vertexBuffer, 0, sizeof(QVector2D));
    glVertexArrayAttribFormat(m_normalizedQuadVao, 0, 2, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(m_normalizedQuadVao, 0, 0);
    glDeleteBuffers(1, &vertexBuffer);

    // Create pointset data resources
    glCreateVertexArrays(1, &m_pointSetVao);
    glEnableVertexArrayAttrib(m_pointSetVao, 0);
    glVertexArrayAttribFormat(m_pointSetVao, 0, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribBinding(m_pointSetVao, 0, 0);

    // Offscreen rendering support
    glCreateTextures(GL_TEXTURE_2D, 1, &m_antialiasingTexture);
    glTextureStorage2D(m_antialiasingTexture, 1, GL_RGBA32F, m_viewport.width(), m_viewport.height());

    glCreateFramebuffers(1, &m_frameBuffer);
    glNamedFramebufferTexture(m_frameBuffer, GL_COLOR_ATTACHMENT0, m_antialiasingTexture, 0);


    // Defered to rendering time to be able to track source changes.
    QDir dirName = QFileInfo(__FILE__).absoluteDir();
    QString vertexSource = QFileInfo(dirName, QStringLiteral("DisplayPointSet.vert")).absoluteFilePath();
    QString fragmentSource = QFileInfo(dirName, QStringLiteral("DisplayPointSet.frag")).absoluteFilePath();
    m_sourceWatcher->addPath(vertexSource);
    m_sourceWatcher->addPath(fragmentSource);
    connect(m_sourceWatcher, &QFileSystemWatcher::fileChanged, [&,w=q->window()]() {
        m_shadersChanged = true;
        w->update();
    });
    m_shadersChanged = true;
}

void Viewer3DPrivate::paint() {
    glPushClientAttrib(GL_CLIENT_ALL_ATTRIB_BITS);
    glPushAttrib(GL_ALL_ATTRIB_BITS);

    glProgramUniform2i(m_drawPointSetProgram, 0, m_viewport.width(), m_viewport.height());
    glViewport(m_viewport.x(), m_viewport.y(), m_viewport.width(), m_viewport.height());

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClearDepth(1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //float zero[] = { 0.0f, 0.0f, 0.0f, 0.0f };
    //glClearTexImage(m_antialiasingTexture, 0, GL_RGBA, GL_FLOAT, zero);
    //glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_frameBuffer);
    glUseProgram(m_drawPointSetProgram);
    glBindVertexArray(m_pointSetVao);
    //glDrawArrays(GL_LINE_STRIP, 0, m_viewport.width());
    glBindVertexArray(0);
    glUseProgram(0);
    //glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

    //glUseProgram(m_antialiasingProgram);
    //glBindVertexArray(m_normalizedQuadVao);
    //glBindTexture(GL_TEXTURE_2D, m_antialiasingTexture);
    //glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    //glBindTexture(GL_TEXTURE_2D, 0);
    //glBindVertexArray(0);
    //glUseProgram(0);

    glPopAttrib();
    glPopClientAttrib();
}

void Viewer3DPrivate::finish()
{
    glDeleteVertexArrays(1, &m_normalizedQuadVao);
    glDeleteTextures(1, &m_antialiasingTexture);
    glDeleteProgram(m_antialiasingProgram);

    glDeleteVertexArrays(1, &m_pointSetVao);
    glDeleteProgram(m_drawPointSetProgram);

    glDeleteFramebuffers(1, &m_frameBuffer);

    m_logger.finish();
}

void Viewer3DPrivate::synchronize()
{
    Q_Q(Viewer3D);

    QRect newViewport = QRect(q->x(), q->window()->height() - q->height() + q->y(), q->width(), q->height());
    bool viewportChanged = m_viewport != newViewport;
    m_viewport = newViewport;

    if (viewportChanged) {
        glDeleteTextures(1, &m_antialiasingTexture);
        glCreateTextures(GL_TEXTURE_2D, 1, &m_antialiasingTexture);
        glTextureStorage2D(m_antialiasingTexture, 1, GL_RGBA32F, m_viewport.width(), m_viewport.height());
        glNamedFramebufferTexture(m_frameBuffer, GL_COLOR_ATTACHMENT0, m_antialiasingTexture, 0);
    }

    if (m_shadersChanged) {
        m_shadersChanged = false;
        glDeleteProgram(m_drawPointSetProgram);
        m_drawPointSetProgram = glCreateProgram();
        GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
        GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

        QDir dirName = QFileInfo(__FILE__).absoluteDir();
        QString vertexSource = QFileInfo(dirName, QStringLiteral("DisplayPointSet.vert")).absoluteFilePath();
        compileShader(vertexSource, vertexShader);
        QString fragmentSource = QFileInfo(dirName, QStringLiteral("DisplayPointSet.frag")).absoluteFilePath();
        compileShader(fragmentSource, fragmentShader);

        m_sourceWatcher->removePath(vertexSource);
        m_sourceWatcher->removePath(fragmentSource);
        m_sourceWatcher->addPath(vertexSource);
        m_sourceWatcher->addPath(fragmentSource);

        glAttachShader(m_drawPointSetProgram, vertexShader);
        glAttachShader(m_drawPointSetProgram, fragmentShader);
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);

        glLinkProgram(m_drawPointSetProgram);
        char buffer[4096];
        GLsizei length;
        glGetProgramInfoLog(m_drawPointSetProgram, sizeof(buffer) - 1, &length, buffer);
        buffer[length] = '\0';
        qDebug() << "curve program link log:\n" << buffer;

        glDeleteProgram(m_antialiasingProgram);
        m_antialiasingProgram = glCreateProgram();
        vertexShader = glCreateShader(GL_VERTEX_SHADER);
        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

        dirName = QFileInfo(__FILE__).absoluteDir();
        vertexSource = QFileInfo(dirName, QStringLiteral("AntiAlias.vert")).absoluteFilePath();
        compileShader(vertexSource, vertexShader);
        fragmentSource = QFileInfo(dirName, QStringLiteral("AntiAlias.frag")).absoluteFilePath();
        compileShader(fragmentSource, fragmentShader);

        m_sourceWatcher->removePath(vertexSource);
        m_sourceWatcher->removePath(fragmentSource);
        m_sourceWatcher->addPath(vertexSource);
        m_sourceWatcher->addPath(fragmentSource);

        glAttachShader(m_antialiasingProgram, vertexShader);
        glAttachShader(m_antialiasingProgram, fragmentShader);
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);

        glLinkProgram(m_antialiasingProgram);
        glGetProgramInfoLog(m_antialiasingProgram, sizeof(buffer) - 1, &length, buffer);
        buffer[length] = '\0';
        qDebug() << "antialias program link log:\n" << buffer;
    }

    if (m_fileNameChanged) {
        m_fileNameChanged = false;


        Assimp::Importer importer;
        GLuint vertexBuffer;
        glCreateBuffers(1, &vertexBuffer);
        QString fileName = q->fileName();
        if (!fileName.isEmpty()) {
            const aiScene* scene = importer.ReadFile(QString("data/" + fileName).toLocal8Bit().constData(), aiProcess_JoinIdenticalVertices);
            for (std::size_t pass = 0; pass < 2; ++pass) {
                // first pass is vertex counting
                // second pass is vertex store

                std::size_t vertexCount = 0;
                for (std::size_t i = 0; i < scene->mNumMeshes; ++i) {
                    const aiMesh* mesh = scene->mMeshes[i];
                    if (pass == 1)
                        glNamedBufferSubData(vertexBuffer, vertexCount * sizeof(QVector3D), mesh->mNumVertices * sizeof(QVector3D), mesh->mVertices);
                    vertexCount += mesh->mNumVertices;
                }
                if (pass == 0)
                    glNamedBufferStorage(vertexBuffer, vertexCount * sizeof(QVector3D), nullptr, GL_DYNAMIC_STORAGE_BIT);
            }

            glVertexArrayVertexBuffer(m_pointSetVao, 0, vertexBuffer, 0, sizeof(QVector3D));
            //glDeleteBuffers(1, &vertexBuffer);
        }
    }

}
