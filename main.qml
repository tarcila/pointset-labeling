import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Window 2.0

import mGlib 1.0

Item {
    id: mainWindow
    visible: true
    width: 800
    height: 600

    Viewer3D {
        id: viewer
        x: 0
        width: parent.width
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        fileName: input.currentText

        ComboBox {
            id: input
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            editable: false
            model: [ "Armadillo.ply", "dragon_vrip.ply", "lucy.ply", "xyzrgb_dragon.ply", "xyzrgb_statuette.ply" ]
        }
    }
}
