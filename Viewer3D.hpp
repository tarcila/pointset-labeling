#pragma once

#include <QPointer>
#include <QQuickItem>

class Viewer3DPrivate;
class Viewer3D : public QQuickItem
{
    Q_OBJECT
    Q_DISABLE_COPY(Viewer3D)

    const QScopedPointer<Viewer3DPrivate> d_ptr;
    Q_DECLARE_PRIVATE(Viewer3D)

    Q_PROPERTY(QString fileName READ fileName MEMBER m_fileName NOTIFY fileNameChanged)

    QString m_fileName;

public:
    explicit Viewer3D(QQuickItem *parent = 0);
    virtual ~Viewer3D();

    QString fileName() const;

Q_SIGNALS:
    void fileNameChanged(QString value);

public Q_SLOTS:
    void handleWindowChanged(QQuickWindow* window);
};
