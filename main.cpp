#include <QGuiApplication>
#include <QQuickView>
#include <QQmlApplicationEngine>

#include "Viewer3D.hpp"
#include <iostream>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<Viewer3D>("mGlib", 1, 0, "Viewer3D");

    QQuickView view;
    view.resize(QSize(800, 600));
    view.setMinimumSize(QSize(400, 300));
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.setSource(QUrl(QStringLiteral("qrc:/main.qml")));
    view.show();

    return app.exec();
}
