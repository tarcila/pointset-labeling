#include "Viewer3D.hpp"
#include "Viewer3DPrivate.hpp"

#include <QQuickWindow>

Viewer3D::Viewer3D(QQuickItem *parent) :
    QQuickItem(parent),
    d_ptr(new Viewer3DPrivate(this))
{
    connect(this, SIGNAL(windowChanged(QQuickWindow*)), this, SLOT(handleWindowChanged(QQuickWindow*)));
}

Viewer3D::~Viewer3D()
{
}

QString Viewer3D::fileName() const
{
    return m_fileName;
}

void Viewer3D::handleWindowChanged(QQuickWindow *window)
{
    Q_D(Viewer3D);

    if (window) {
        d->doConnect();
        window->setClearBeforeRendering(false);
    } else {
        d->doDisconnect();
    }
}
