#include "OpenGLLogger.hpp"
#include "OpenGLLoggerPrivate.hpp"

OpenGLLoggerPrivate::OpenGLLoggerPrivate(OpenGLLogger *parent) :
    QObject(parent),
    q_ptr(parent),
    m_logger(parent)
{
}

OpenGLLoggerPrivate::~OpenGLLoggerPrivate()
{
}

void OpenGLLoggerPrivate::initialize()
{
    m_logger.initialize();
    connect(&m_logger, SIGNAL(messageLogged(QOpenGLDebugMessage)), this, SLOT(logMessage(QOpenGLDebugMessage)));
    m_logger.startLogging(QOpenGLDebugLogger::SynchronousLogging);
}

void OpenGLLoggerPrivate::finish()
{
    m_logger.stopLogging();
}

void OpenGLLoggerPrivate::logMessage(const QOpenGLDebugMessage &message)
{
    qDebug() << message;
}
