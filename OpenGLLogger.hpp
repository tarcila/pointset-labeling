#pragma once

#include <QObject>
#include <QPointer>

class OpenGLLoggerPrivate;

class OpenGLLogger : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(OpenGLLogger)


    const QScopedPointer<OpenGLLoggerPrivate> d_ptr;
    Q_DECLARE_PRIVATE(OpenGLLogger)

public:
    explicit OpenGLLogger(QObject *parent = 0);
    virtual ~OpenGLLogger();

    void initialize();
    void finish();
};
