#include "Filter.cu.h"

#include <cuda_runtime.h>

#define BLOCK_SIZE 256

__global__ void cudaComputeReduction(const int outputSize, PositionAndColor* output, int* weights, const int inputSize, const float2* input, const float2 inputRange, const int colormapSize, const float4* colormap) {
}

void computeReduction(const int outputSize, PositionAndColor* output, const int inputSize, const float2* input, const float2 inputRange, const int colormapSize, const float4* colormap) {

}
