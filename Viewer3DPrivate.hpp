#pragma once

#include <GL/glew.h>
#include "Viewer3D.hpp"
#include "OpenGLLogger.hpp"
#include "Filter.cu.h"

#include <QFileSystemWatcher>
#include <QObject>
#include <QOpenGLShader>

#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

class Viewer3DPrivate : public QObject {
    Q_OBJECT
    Q_DISABLE_COPY(Viewer3DPrivate)

    Viewer3D* const q_ptr;
    Q_DECLARE_PUBLIC(Viewer3D)

    QRect m_viewport;

    // Offscreen rendering
    GLuint m_frameBuffer;

    // Curve drawing
    GLuint m_drawPointSetProgram;
    GLuint m_pointSetVao;

    // Post processing
    GLuint m_antialiasingProgram;
    GLuint m_antialiasingTexture;
    GLuint m_normalizedQuadVao;
    OpenGLLogger m_logger;

    bool m_shadersChanged;
    QFileSystemWatcher* m_sourceWatcher;

    bool m_fileNameChanged;

public:
    Viewer3DPrivate(Viewer3D* parent);

    void doConnect();
    void doDisconnect();

protected:
    void compileShader(QString fileName, GLuint shaderObject);

protected Q_SLOTS:
    void init();
    void paint();
    void finish();
    void synchronize();
};
