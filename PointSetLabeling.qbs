import qbs

Project {
    qbsSearchPaths: "qbs/"

    Application {
        Depends { name: "cpp" }

        Properties {
            condition: qbs.toolchain.contains("gcc") && !qbs.toolchain.contains("clang")
            cpp.cxxFlags: ["-std=c++14"]
        }

        Properties {
            condition: qbs.toolchain.contains("clang")
            cpp.cxxFlags: ["-std=c++14", "-stdlib=libc++"]
            cpp.linkerFlags: ["-stdlib=libc++", "-pthread"]
            cpp.dynamicLibraries: ["c++abi"]
        }

        // Qt support
        Depends { name: "Qt"; submodules: ["core", "gui", "opengl", "quick"]; required: true }

        // CUDA support
        Depends { name: "CUDA"; submodules: ["nvcc", "runtime"]; required: true }

        //Depends { name: "OpenGL" }

        cpp.dynamicLibraries: [ "GL", "GLEW", "assimp" ]
        files: [
            "Filter.cu",
            "Filter.cu.h",
            "Viewer3D.cpp",
            "Viewer3D.hpp",
            "main.cpp",
            "OpenGLLogger.cpp",
            "OpenGLLogger.hpp",
            "OpenGLLoggerPrivate.cpp",
            "OpenGLLoggerPrivate.hpp",
            "qml.qrc",
            "Viewer3DPrivate.cpp",
            "Viewer3DPrivate.hpp",
        ]

        Group {
            fileTagsFilter: product.type
            qbs.install: true
        }
    }

}
